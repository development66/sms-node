const { Router } = require("express");
const router = Router();

const { sendMessage } = require("../twilio/send-sms");
const SMS = require('../models/sms');

router.get("/", async (req, res) => {
  const messages = await SMS.find().lean();
  messages.forEach(m => console.log(m));
  
  res.render("index", {messages});
});

router.post("/send-sms", async (req, res) => {
  console.log(req.body);
  const {message, phone} = req.body;

  if(!message || !phone) return res.json('Missing message or phone');

  const response = await sendMessage(message, phone);
  await SMS.create({
    Body: message,
    To: phone
  })
  console.log(response.sid);
  res.redirect("/");
});

module.exports = router;
