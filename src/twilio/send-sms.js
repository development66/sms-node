const config = require('../config');
const client = require('twilio')(config.accountSid, config.authToken);

/**
 * Send message sms
 * @param {string} body - the sms message
 * @param {number} phone - the phone number
 */

async function sendMessage(body, phone) {
    try {
        const message = await client.messages.create({
            to: phone,
            from: '+12513571217',
            body
        });
        return message;
    } catch (error) {
        console.log(error);
    }
}

module.exports = {sendMessage};