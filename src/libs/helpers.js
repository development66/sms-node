module.exports = {
    hideNumber: (phone = '') => {
        return phone.replace(/[0-9]/g, 'x');
    }
}