require('dotenv').config();

console.log(process.env.PORT);

const app = require('./server');

require('./database');

app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});